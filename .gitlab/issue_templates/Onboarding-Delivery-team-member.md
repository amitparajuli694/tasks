## New team member

Welcome to the Delivery team!

The information below is meant to serve as a quick overview of both the company and the team resources.

Feel free to skip parts you are already familiar with, but keep in mind that some of the resources might have changed since you read them so a quick refresher could be useful.

## Company
GitLab is organised based on the product categories.  Also have a look at the team org chart.

## Team
Like all teams we have a Delivery team section in the handbook. Take some time to read through, including all the linked resources.

Team member onboarding tasks
- [ ] Read the [Delivery Team](https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery/) handbook section
- [ ] Review the [Infrastructure Department](https://about.gitlab.com/handbook/engineering/infrastructure/) handbook section
- [ ] Join the following Slack channels:
   - [ ] #eng-week-in-review (mandatory channel for engineering)
   - [ ] #g_delivery (team channel)
   - [ ] #delivery_lounge (Delivery team social chats)
   - [ ] #releases
   - [ ] #f_upcoming_release
   - [ ] #announcements (deployment feed, and general infrastructure announcements)
   - [ ] #production
   - [ ] #incident-management
   - [ ] #infrastructure-lounge  (Infrastructure department channels)
- [ ] Read the [keeping yourself informed documentation](https://about.gitlab.com/handbook/marketing/strategic-marketing/getting-started/communication/)
- [ ] [Update the team page](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page)

## Manager onboarding tasks
For all team members:
- [ ] Add new team member to delivery-team@gitlab.com Google group
- [ ] Add new team member to [Delivery group](https://gitlab.com/groups/gitlab-org/delivery/-/group_members?with_inherited_permissions=exclude) on GitLab.com
- [ ] Create an access request to add to the delivery-team slack group
- [ ] Create access request to 'Release' and 'Build' vaults
- [ ] Add to weekly team meeting
- [ ] Create a 1-1 document and schedule a recurring weekly meeting

Role specific access requests
- [ ] Create an access request for Managers, Infrastructure baseline entitlement
- [ ] Create an access request for Backend, Infrastructure baseline entitlement
- [ ] Create an access request for SRE, Infrastructure entitlement

## Training resources

- [ ] Overall release process can [be found here](https://about.gitlab.com/handbook/engineering/releases)
  * Our code is deployed to Gitlab.com in certain intervals this process is called auto deploy. [Auto deploy process details can be found in this document](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/auto-deploy.md)
  * There are 3 type of patch
    * When there is a security related issue we need to release a patch to fix the issue. [This](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/release-manager.md) documentation outlines what are the steps RM needs to take during that process.
    * We sometimes ship buggy code, we need to create patch that includes so that our on premise customers can apply to their instance. Process for patch release can be found [here](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/process.md)
    * [Hot Patch deployment process](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/post-deployment-patches.md). This process is literally applying patches to rails and restarting puma. HotPatch is not a normal release we do it in exceptional situation to make Gitlab.com available.
* [Runbook section](https://gitlab.com/gitlab-org/release/docs/-/tree/master/#runbooks) of our documentation will cover scenarios and action items.
* [What should RM do in case of failure during auto deploy process](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/failures.md)
* \[Most of the release related documents can be found in [this](https://gitlab.com/gitlab-org/release/docs/-/tree/master/) repository
* https://gitlab.com/gitlab-org/release/docs/-/tree/master/release_manager
* https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/dashboard.md
* https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/big_picture.md

### Important Projects to check

Please check following projects documentation

* Gitlab is packaged through https://gitlab.com/gitlab-org/omnibus-gitlab/ project.
* Release operations are triggered through [ChatOps](https://gitlab.com/gitlab-com/chatops) project. You can see what kind of commands available by issuing `/chatops run help`
* [Release tools](https://gitlab.com/gitlab-org/release-tools) are used to operate releases. We're using a mixture of GitLab API calls and Delivery-grown code
* [Deploy tooling](https://gitlab.com/gitlab-com/gl-infra/deploy-tooling/) Holds ansible playbooks for deploying Gitlab to VMs
* [Deployer](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/gitlab-com-deployer.md) this project runs GitLab.com deployment pipelines
* [release-tasks](https://gitlab.com/gitlab-org/release/tasks/issues) - issues related to releases and deployments

### Dashboards

* [Dashboard showing resource utilization between virtual machines and the Kubernetes clusters for GitLab.com](https://dashboards.gitlab.net/d/delivery-k8s_migration_overview/delivery-kubernetes-migration-overview?orgId=1&refresh=5m)
* [Release manager dashboard](https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/dashboard.md)
* [Omnibus versioning dashboard](https://dashboards.gitlab.net/d/CRNfDC7mk/gitlab-omnibus-versions?orgId=1&refresh=5m)
